console.log('Written by Eli Gladman (eli.gladman.xyz)');

// Given more time I would have heavily refactor the code and cache the API responses.
// I chose to write vanilla JavaScript becasue it seemed like overkill to use a framework.

var width = window.innerWidth * 0.85,
    height = window.innerHeight * 0.85;

var svg = d3.select("body").append("svg")
            .attr('width', width)
            .attr('height', height);

var file = 'sfmaps/streets.json';

function renderMap() {
  d3.json(file, function(json) {

    var center = d3.geo.centroid(json)
    var scale  = 150;
    var offset = [width/2, height/2];

    projection = d3.geo.mercator()
                 .scale(scale)
                 .center(center)
                 .translate(offset);

    var path = d3.geo.path().projection(projection);

    var bounds  = path.bounds(json);
    var hscale  = scale*width  / (bounds[1][0] - bounds[0][0]);
    var vscale  = scale*height / (bounds[1][1] - bounds[0][1]);
    var scale   = (hscale < vscale) ? hscale : vscale;
    var offset  = [
      width - (bounds[0][0] + bounds[1][0])/2,
      height - (bounds[0][1] + bounds[1][1])/2
    ];

    // new projection
    projection = d3.geo.mercator().center(center)
      .scale(scale).translate(offset);

    path = path.projection(projection);

    svg.selectAll("path").data(json.features).enter().append("path")
       .attr("d", path);
     });

}




function fetchLocation() {
  var xhr = new XMLHttpRequest();

  wait = true;


  var time = 0;
  var agencyTag = 'sf-muni';

  var url = 'http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=' + agencyTag + '&t=' + time;

  xhr.open('GET', url, true);

  xhr.onload = function() {
    if (xhr.status >= 200 && xhr.status < 400) {
      // Success!
      var response = xhr.responseText;

      if (window.DOMParser) {
        var parser = new DOMParser();
        var xml = parser.parseFromString(response,"text/xml");

      // console.log(response);

      } else {
        xml = new ActiveXObject("Microsoft.XMLDOM");
        xml.async=false;
        xml.loadXML(response);
      }

      var vehicles = xml.getElementsByTagName("vehicle");

      for (var i = 0; i < vehicles.length; i++) {
        var latitude = vehicles[i].getAttribute('lat');
        var longitude = vehicles[i].getAttribute('lon');

        coordinates = projection([longitude, latitude]);

        svg.append('svg:circle')
        .attr('cx', coordinates[0])
        .attr('cy', coordinates[1])
        .attr('r', 3);

      }

    } else {
      console.log('ERROR ' + xhr.status + ': failed to retrieve data from NextBus API');
    }

  };

  xhr.onerror = function() {
    console.log('Whoa . . . a network error occured');
  };

  xhr.send();

}


renderMap();

(function() {
  var el = document.getElementsByTagName("path");

  if (el.length > 0) {
    fetchLocation();
  } else {
    setTimeout(fetchLocation, 2500);
  }

})();





setInterval( function() {
  var circles = document.getElementsByTagName("circle");

  // remove previous vehicle locations from map
  if (circles.length > 0) {
    for (i = circles.length - 1; i >= 0; i--) {
      circles[i].parentNode.removeChild(circles[i]);
    }
  }

  // console.log('updated');
  fetchLocation();

}, 15000);




 function fetchRoutes() {
   var xhr = new XMLHttpRequest();

   var epochTime = (new Date).getTime(),
       agencyTag = 'sf-muni';

   var url = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=' + agencyTag;

   xhr.open('GET', url, true);

   xhr.onload = function() {
     if (xhr.status >= 200 && xhr.status < 400) {
       // Success!
       var resp = xhr.responseText;
       console.log(resp);
     } else {
       // We reached our target server, but it returned an error

     }
   };

   xhr.onerror = function() {
     // There was a connection error of some sort
   };

   xhr.send();

  }

  // fetchRoutes();










